const locators = {
    LOGIN:{
        USER: '[id="user-name"]',
        PASSWORD: '[id="password"]',
        LOGIN_BUTTON: '[id="login-button"]'
    }
}
export default locators;