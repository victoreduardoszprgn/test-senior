# Test Senior


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/victoreduardoszprgn/test-senior.git
git branch -M main
git push -uf origin main
```

## Tools Used

- The latest version of Cypress was used. Version 13.3.0
    Integrated together with TypeScript to use Locators.
    Where it assists in maintaining the HTML of tests and reusing them


## CI/CD
- The CI/CD job is called Testing.
Tests created via terminal are run on it.
